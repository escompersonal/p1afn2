/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p1afn2;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author carlos
 */
public class P1AFN2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ControladorArchivo ca = new ControladorArchivo();
        List<String> aceptacion = new ArrayList<String>();  
        

        String inicial = ca.obtenerInicial();
        aceptacion = ca.obtenerEdosAceptacion();
        TablaTrans tabla = ca.obtenerTabla();

        Automata a = new Automata(tabla, inicial, aceptacion);
        boolean rep = true;
        while (rep) {
            System.out.println("-------------------------------------------------------------");
            String cadena = JOptionPane.showInputDialog("Cadena a validar: ");
            if(!(a.validarCadena(cadena))){
                System.out.println("Cadena no valida");
            }
            System.out.println("-------------------------------------------------------------");
            int ax = JOptionPane.showConfirmDialog(null, "Validar otra cadena?", "Repetir validacion", JOptionPane.YES_NO_OPTION);
            if (!(ax == JOptionPane.YES_OPTION)) {
                rep = false;
            }

        }

    }

}
