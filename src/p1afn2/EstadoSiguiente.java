/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p1afn2;

/**
 * Clase auxiliar para guardar el recorrido 
 * @author carlos
 */
public class EstadoSiguiente {
    
    String cad; //cadena con la cual se llego a este estado
    String q; //Nombre del estado
    
    /**
    * @param cad: Inicializa la cade que le corresponde al estado 
    */
    public void setCad(String cad) {
        this.cad = cad;
    } 
    
    /**
    * @param q: nombre o identificador del estado
    */
    public void setQ(String q) {
        this.q = q;
    }
    
    /**
    * Se sobrecarga el metodo Object.toString() imprimir con print
    * @return Informacion que contiene el objeto
    * @see Object#toString() 
    */
    @Override
    public String toString() {
        return "("+cad+") -> "+q;
    }
    
    
    
    
}
