/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p1afn2;

import java.util.ArrayList;

/**
 * Lista personalizada para guardar Objetos tipo Tansicion
 * @author carlos
 * @see Transicion
 */
public class TablaTrans extends ArrayList<Transicion>{    
    
    /**
    * @param t: transicion que se va a agregar a la lista
    * @return no se
    * @see ArrayList#add(java.lang.Object) 
    */    
    @Override
    public boolean add(Transicion t){
        return super.add(t);        
    }
    
    /**
    * 
    * @param i: Indice de elemento que se quiere obtener
    * @return Devuelve la Transicion en el indice indicado
    * @see ArrayList#get(int) 
    */  
    @Override
    public Transicion get(int i){
        return super.get(i);        
    }
    
    /**
     *@return Devuelve la cantidad de elementos en la lista
     *@see  ArrayList#size()  
     */    
    @Override
    public int  size(){
        return super.size();
    }    
    
}
