/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p1afn2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlos
 */
public class ControladorArchivo {

    private LectorFichero lf;//clase que lee el archivo de entrada
    
    
    
    public ControladorArchivo() {
        lf = new LectorFichero("in");//se le pasa la ruta del archivo de entrada
       
    }
    
    
    /**
     * Lee rrecibe del lectro los estado de aceotacion y coloca en True el arreglo en la posicion del estado
     * @return regresa el arreglo poniendo True en la posicion correspondiente
     */

    public List<String> obtenerEdosAceptacion() {
        List<String> aceptacion = new ArrayList<String>();  
        
        try {
            String[] aux = (lf.obtenerEstadosFinales()).split(",");//Separa la cadena  obtenida, por comas
            for (int i = 0; i < aux.length; i++) {//recorre el arreglo para colocar el valor en el sitio correpondiente
                aceptacion.add(aux[i]);
            }

            return aceptacion;
        } catch (IOException ex) {
            Logger.getLogger(ControladorArchivo.class.getName()).log(Level.SEVERE, null, ex);
        }
       return aceptacion;
    }
    /**
     * Obtiene del archivo la linea con el estado inicial
     * @return Regresa el esatdo inicial como cadena
     */
    public String obtenerInicial() {
        try {
            lf.obtenerEstados();//lee las dos primeras lineas cuyos datos no se usan
            lf.obtenerAlfabeto();
            return lf.obtenerEstadoInicial();//lee le la linea donde se encuentra el estado final
        } catch (IOException ex) {
            Logger.getLogger(ControladorArchivo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    /**
     * Lee todas las lineas que contiene las transiciones y genera el Objeto TablaTrans
     * @see TablaTrans
     * @return Devuelve la tabla generada
     */
    public TablaTrans obtenerTabla() {
        TablaTrans tabla = new TablaTrans();
        String[] trans;
        try {

            while ((trans = lf.obtenerTransicion()) != null) {//Lee el archivo hasta final del achivo linea a linea
                Transicion t = new Transicion();//Por cada linea se genera un objeto Transicio y se agrega a la lista
                t.setOrigen(trans[0]);
                t.setCadena(trans[1]);
                t.setDestino(trans[2]);
                tabla.add(t);//Se agrega la Transicion a la lista
            }
            return tabla;
        } catch (IOException ex) {
            Logger.getLogger(ControladorArchivo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tabla;
    }

}
