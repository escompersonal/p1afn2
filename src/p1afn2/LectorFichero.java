/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p1afn2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Realiza la lectura del archivo y devuelve los valores como cadenas
 * @author carlos 
 */
public class LectorFichero {
    private String path; 
    private File file;
    FileReader fr;
    BufferedReader br;
    /**
    *Recibe la ruta del fichero y abre el flujo de lectura
    *@param path : ruta donde se encuentra el fichero a leer
    */
    
    public LectorFichero(String path){
        this.path = path;
        
        try {
            file = new File(path);
            fr = new FileReader (file);
            br = new BufferedReader(fr);
        } catch (FileNotFoundException ex) {
            cerrarFichero();
        }
       
        
    }
    /**
    *
    * @return regresa la linea que contien los estados
     * @throws java.io.IOException
    */
    public String obtenerEstados() throws IOException{
        return br.readLine();
    }
    
    /**
    *
    * @return regresa la linea que contien el alfabeto
    */    
    public String obtenerAlfabeto() throws IOException{
        return br.readLine();
    }
    
    /**
    *
    * @return regresa la linea que contiene el estado incical
     * @throws java.io.IOException
    */
    public String obtenerEstadoInicial() throws IOException{
        return br.readLine();   
    }
    
    /**
    *
    * @return regresa la linea que contiene los estados finales o de apcetacion
     * @throws java.io.IOException
    */
    public String obtenerEstadosFinales() throws IOException{
        return br.readLine();   
    }
    
    /**
    *
    * @return regresa una linea que contiene una transicion
     * @throws java.io.IOException
    */
    public String[] obtenerTransicion() throws IOException{
        String linea;
        String [] trans;
        if((linea = br.readLine())!=null){
            trans = linea.split(",");
        }            
        else
            trans =null;
        
        return trans;        
    }
    
    /**
    *
    * cierra el fichero
    */
    
    public void cerrarFichero(){
        try {  
           if( null != fr ){   
               fr.close();     
            } 
        } catch (IOException ex) {
            
        }
    }
    
}
